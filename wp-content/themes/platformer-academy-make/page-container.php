<?php 


/**
 * Template Name: Section/Container
 *
 *
 * @package Platformer
 * @since 1.0
 */


get_header(); 

?>
<!-- PAGE-Section/Container -->
<div class="section c_blue">
    <div class="w-container">
      <h1 class="page_header"><?php wp_title('') ?></h1>
    </div>
</div>
<div class="section min_vh">
		<div class="w-container">
			<?php
					if ( have_posts() ) :
		
						/* Start the Loop */
						while ( have_posts() ) : the_post();
		
					        the_content();
		
						endwhile;
		
					endif;
			?>			
		</div>
	</div>
<?php get_footer();
