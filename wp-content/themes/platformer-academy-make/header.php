<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta content="width=device-width, initial-scale=1" name="viewport">      
    <title><?php wp_title(''); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({  google: {    families: ["Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Libre Baskerville:regular,italic,700","Raleway:regular","Arvo:regular","Roboto:regular","Poppins:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Playfair Display:regular,italic,700,700italic,900,900italic","Roboto Slab:100,300,regular,700","Abril Fatface:regular"]  }});</script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
    <!--<link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">-->
    <!--<link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">-->
<script type="text/javascript">
      var _dcq = _dcq || [];
      var _dcs = _dcs || {};
      _dcs.account = '4164281';
    
      (function() {
      var dc = document.createElement('script');
      dc.type = 'text/javascript'; dc.async = true;
      dc.src = '//tag.getdrip.com/4164281.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(dc, s);
      })();
</script>
<!--End Drip-->
<!-- RightMessage -->


    <?php wp_head(); ?>
    
    <!-- RightMessage -->
<script type="text/javascript">
(function(p, a, n, d, o) {
    a = document.createElement('script');
    a.type = 'text/javascript'; a.async = true; a.src = p;
    n = document.getElementsByTagName('script')[0]; n.parentNode.insertBefore(a, n);
})('https://tag.rightmessage.com/620200330.js');
</script>
</head>

<body>
    
<?php if (is_user_logged_in() ) : ?>
    <?php get_template_part('template-parts/nav/logged_in_nav'); ?>
<?php else: ?>
    <?php get_template_part('template-parts/nav/logged_out_nav'); ?>
<?php endif; ?>    
    