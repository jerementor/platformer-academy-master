<?php 

/**
 * The template for displaying all blog posts
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
  <div class="section padded_section min_vh">
    <div class="max_width">
      <div class="container w-container">
            <h2 class="blog_title"><?php wp_title(''); ?></h2>
            <h4 class="blog_author">By Jeremy Alexander</h4>
              <?php
                			if ( have_posts() ) :
   
                				while ( have_posts() ) : the_post();
                
                			        the_content();
                              comments_template();
                				endwhile;
                
                			endif;
              ?>
      </div>    
    </div>    
  </div>    

<?php get_footer(); ?>

