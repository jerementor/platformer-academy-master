<?php
/**
 * The sidebar containing the course menu
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Platformer
 * @version 1.0.0
 */
?>


    <div class="course_sidebar">
        <aside id="secondary" role="complementary">
            
            <div class="lecture_sidebar">
            <h4 class="sidebar_toc">Table of Contents</h4>
            
            <?php
                if(is_active_sidebar('course-sidebar')){
                    dynamic_sidebar('course-sidebar');
                }
            ?>
          
            </div>
        </aside>   
    </div>


