<?php 

/**
 * The template for displaying all pages
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<!--INDEX-->
<div class="section c_blue">
    <div class="w-container">
      <h1 class="page_header"><?php wp_title('') ?></h1>
    </div>
</div> 
<div class="section min_vh">
	<div class="w-container">
			<?php if (have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<a href="<?php the_permalink(); ?>" class="blog_link">
								<div class="blog_card">
									<?php if (has_post_thumbnail( $post->ID ) ): ?>
										<?php the_post_thumbnail(); ?>
									<?php endif; ?>
									<div class="blog_card_body">
										<h2><?php the_title(); ?></h2>
										<p><?php the_excerpt(); ?></p>
									</div>
								</div>
							</a>
				<?php endwhile; ?>
			<?php endif; ?>
	</div>
</div>	

<?php get_footer();
