<?php 

/**
 * The template for displaying all pages
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<!--PAGE-->
	<?php
			if ( have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) : the_post();

			        the_content();

				endwhile;

			endif;
	?>
	
<?php get_footer();
