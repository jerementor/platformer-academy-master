 <div data-collapse="medium" data-animation="default" data-duration="400" class="nav_bar w-nav">
    <div class="w-container"><a href="/" class="w-clearfix w-nav-brand"><img src="/wp-content/uploads/2017/11/wizard_guy.gif" class="image-6"><h4 class="heading-3">Platformer Academy</h4></a>
      <nav role="navigation" class="w-nav-menu">
            <?php get_template_part( 'template-parts/nav/nav_out'); ?>
      </nav>
      <div class="w-nav-button">
        <div class="w-icon-nav-menu"></div>
      </div>
    </div>
  </div>