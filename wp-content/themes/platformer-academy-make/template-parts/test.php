<div class="section">
	<div class="w-container">
			<?php $i = 0; ?>			
			<?php if (have_posts() ) : while ( have_posts() ) : the_post(); ?>
					
					<?php
						if($i ==0){
							echo '<div class="w-row">';
						}
					?>
							<div class="w-col w-col-6">
								<a href="<?php the_permalink(); ?>" class="blog_link w-inline-block">
									<div class="blog_card">
										<img src="<?php the_post_thumbnail(); ?>" ></img>
										<div class="blog_card_body">
											<h2><?php the_title(); ?></h2>
											<p><?php the_excerpt(); ?></p>
										</div>
									</div>
								</a>
							</div>
		<?php
			$i++;
			if($i == 2){
				$i = 0;
				echo '</div>';
			}
		?>
				
			<?php endwhile; ?>
			<?php endif; ?>
		<!--</div>-->
		<?php
			if($i > 0){
				echo '</div>';
			}
		
		?>
	</div>
</div>	