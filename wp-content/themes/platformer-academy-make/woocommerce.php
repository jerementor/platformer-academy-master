<?php 

/**
 * The template for displaying all pages
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<!--WOOCOMMERCEPHP-->

		<?php woocommerce_content(); ?>

	
<?php get_footer();
