<?php

//Loading in Scripts
function plat_enqueue_styles(){
    wp_enqueue_script('webflow-js', get_template_directory_uri() . '/assets/js/webflow.js', array('jquery'));

    
    wp_enqueue_style("normalize",  get_template_directory_uri() . "/assets/css/normalize.css");
    wp_enqueue_style("webflow",  get_template_directory_uri() . "/assets/css/webflow.css");
    wp_enqueue_style("platformeracademy",  get_template_directory_uri() ."/assets/css/platformer-make.webflow.css");
    wp_enqueue_style("platformeracademy-style", get_stylesheet_uri());
    
}
add_action("wp_enqueue_scripts", "plat_enqueue_styles");

function plat_menu_setup(){
    
    //Registers custom primary nav menu 
    register_nav_menus( array(
        'primary'       => __("Primary Menu", "platformeracademy"),
        'logged-in'     => __("Logged-In Menu", "platformeracademy-login"),
        'logged-out'    => __("Logged-Out Menu", "platformeracademy-logout"),
        'dash-board'     => __("Dashboard Menu", "platformeracademy-dashboard"),
    ));
    
}
add_action("after_setup_theme", "plat_menu_setup");

function platformeracademy_setup(){
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	
	) );
}
add_action( 'after_setup_theme', 'platformeracademy_setup' );


// remove_filter('the_content', 'wpautop');
add_post_type_support( 'page', 'excerpt' );

function add_custom_post_type_to_query( $query ) {
    if ( $query->is_home() && $query->is_main_query() ) {
        $query->set( 'post_type', array('post', 'video') );
    }
}
add_action( 'pre_get_posts', 'add_custom_post_type_to_query' );


function plat_widgets(){
  register_sidebar(array(
     'name'           => __('Course - Sidebar', 'platformeracademy'),
     'id'             => 'course-sidebar',
     'description'    => __('Add widgets here to appear in the sidebar', 'platformeracademy'),
     'before_widget'  => '<div id="%1$s" class="%2$s">',
     'after_widget'   => '</div>',
     'before_title'   => '<h3 class="course_section"> ',
     'after_title'    => '</h3>',
  ));

  
  
    
}
add_action('widgets_init', 'plat_widgets');


// WooCommerce Support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

//remove certain menus
add_filter ( 'woocommerce_account_menu_items', 'plat_remove_my_account_links' );
function plat_remove_my_account_links( $menu_links ){
 
	unset( $menu_links['edit-address'] ); // Addresses
    unset( $menu_links['dashboard'] ); // Dashboard
	//unset( $menu_links['payment-methods'] ); // Payment Methods
	//unset( $menu_links['orders'] ); // Orders
	//unset( $menu_links['downloads'] ); // Downloads
	//unset( $menu_links['edit-account'] ); // Account details
	//unset( $menu_links['customer-logout'] ); // Logout
 
	return $menu_links;
 
}

//remove checkout fields
add_filter( 'woocommerce_checkout_fields' , 'plat_disable_address_fields_validation' );
function plat_disable_address_fields_validation( $address_fields_array ) {
 
	unset( $address_fields_array['billing']['billing_state']);
 	unset( $address_fields_array['billing']['billing_postcode']);
 	unset( $address_fields_array['billing']['billing_company']);
 	unset( $address_fields_array['billing']['billing_address_1']);
 	unset( $address_fields_array['billing']['billing_address_2']);
	unset( $address_fields_array['billing']['billing_city']);
	unset( $address_fields_array['billing']['billing_phone']);
	unset( $address_fields_array['billing']['billing_country']);
	// you can also hook first_name and last_name, company, country, city, address_1 and address_2
 
	return $address_fields_array;
 
}

// removes Order Notes Title - Additional Information
add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );



//remove Order Notes Field
add_filter( 'woocommerce_checkout_fields' , 'plat_remove_order_notes' );

function plat_remove_order_notes( $fields ) {
     unset($fields['order']['order_comments']);
     return $fields;
}

//redirect shop to products
add_filter( 'woocommerce_return_to_shop_redirect', "plat_woocommerce_return_to_shop_redirect" ,20 );
function plat_woocommerce_return_to_shop_redirect(){
   return site_url()."/products";
}

// hide coupon field on cart page
function plat_hide_coupon_field_on_cart( $enabled ) {
	if ( is_cart() ) {
		$enabled = false;
	}
	return $enabled;
}
add_filter( 'woocommerce_coupons_enabled', 'plat_hide_coupon_field_on_cart' );


/*----------------------------------------------------------------------------*/
// redirects for login / logout
/*----------------------------------------------------------------------------*/
add_filter('woocommerce_login_redirect', 'plat_login_redirect');

function plat_login_redirect($redirect_to) {

    // return home_url();
    return site_url()."/your-courses";

}

add_action('wp_logout','plat_logout_redirect');

function plat_logout_redirect(){

    // wp_redirect( home_url() );
     return site_url()."/products";
    // exit;

}

//redirect on purchase
add_action( 'woocommerce_thankyou', 'plat_redirect_custom');
 
function plat_redirect_custom( $order_id ){
    $order = new WC_Order( $order_id );
 
    $url = site_url()."/your-courses";
 
    if ( $order->status != 'failed' ) {
        wp_redirect($url);
        exit;
    }
}

//redirect shop page
function plat_shop_page_redirect() {
    if( is_shop() ){
        wp_redirect( home_url( '/products/' ) );
        exit();
    }
}
add_action( 'template_redirect', 'plat_shop_page_redirect' );

// remove succesfully added to cart
//add_filter( 'wc_add_to_cart_message_html', '__return_null' );

// check for clear-cart get param to clear the cart, append ?clear-cart to any site url to trigger this
add_action( 'init', 'plat_woocommerce_clear_cart_url' );
function plat_woocommerce_clear_cart_url() {
	if ( isset( $_GET['clear-cart'] ) ) {
		global $woocommerce;
		$woocommerce->cart->empty_cart();
	}
}

//remove product link on order review woo
add_filter( 'woocommerce_order_item_name', 'plat_remove_permalink_order_table', 10, 3 );
 
function plat_remove_permalink_order_table( $name, $item, $order ) {
   $name = $item['name'];
   return $name;
}


//go straight to checkout
// add_filter('woocommerce_add_to_cart_redirect', 'plat_add_to_cart_redirect');
// function plat_add_to_cart_redirect() {
//  global $woocommerce;
//  $checkout_url = wc_get_checkout_url();
//  return $checkout_url;
// }

//remove cart thumbnails
add_filter( 'woocommerce_cart_item_thumbnail', '__return_false' );



// remove cart page links
function plat_remove_cart_product_link( $product_link, $cart_item, $cart_item_key ) {
    $product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    return $product->get_title();
}
add_filter( 'woocommerce_cart_item_name', 'plat_remove_cart_product_link', 10, 3 );

?>



