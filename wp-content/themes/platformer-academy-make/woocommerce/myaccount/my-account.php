<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

/**
 * My Account navigation.
 * @since 2.6.0
 */
?>
<!--MY ACCOUNT PHP WOOCOMMERCE PA-->
<!--<div class="section c_blue">-->
<!--    <div class="w-container">-->
<!--      <h1 class="page_header"><!?php wp_title('') ?></h1>-->
<!--    </div>-->
<!--</div> -->

<!--Dash Nav-->
<!--<!?php if (is_user_logged_in() ) : ?>-->
<!--    <!?php get_template_part('template-parts/nav/dash_nav'); ?>-->
<!--<!?php endif; ?>    -->
    

<div class="section padded_section min_vh">
    <div class="container w-container">
				<?php
					/**
					 * My Account content.
					 * @since 2.6.0
					 */
					do_action( 'woocommerce_account_content' );
				?>
  
			  <!--<!?php do_action( 'woocommerce_account_navigation' ); ?>-->
  </div>
</div>

