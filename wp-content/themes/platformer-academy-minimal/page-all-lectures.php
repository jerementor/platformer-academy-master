<?php 

/**
 * Template Name: All Lectures Page
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<body>
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            
            <div class="plat_flex_sidebar">
            <div class="plat_wp_sidebar">
                <h2 class="heading-9">Table of Contents</h2>
                    <?php get_sidebar('course-sidebar'); ?>

            </div>
            <div class="plat_all_content">
                <?php the_content(); ?>
            </div>
            </div>
     

        <?php endwhile; ?>
    <?php endif; ?>            


    
<?php get_footer(); ?>
 