<?php 

/**
 * Template Name: DPT Page
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<body>

  <div class="plat_minvh_section plat_padding_section">
    <div class="w-container">
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            
           <!--<!?php the_title(sprintf('<a class="plat_content_header" href="%s" rel="bookmark">', esc_url(get_permalink() )), '</a>'); ?>-->
            <?php the_content(); ?>

        <?php endwhile; ?>
    <?php endif; ?>            
    </div>
    </div>
     
    
<?php get_footer(); ?>