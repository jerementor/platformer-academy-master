<!--<div data-collapse="medium" data-animation="default" data-duration="400" class="plat_navbar w-nav">-->
<!--  <div class="w-container">-->
<!--    <a href="/" class="w-nav-brand">-->
<!--      <h1 class="plat_text_logo">Platformer Academy</h1>-->
<!--    </a>-->
<!--    <nav role="navigation" class="nav-menu w-nav-menu">-->
<!--    <!?php get_template_part( 'template-parts/nav/plat_nav_out'); ?>-->
     
<!--    </nav>-->
<!--    <div class="menu-button w-nav-button">-->
<!--      <div class="w-icon-nav-menu"></div>-->
<!--    </div>-->
<!--  </div>-->
<!--</div>-->

<div data-collapse="medium" data-animation="default" data-duration="400" class="plat_blue_nav w-nav">
  <div class="w-container">
    <a href="/" class="w-nav-brand">
      <h4 class="plat_inverse_brand"><strong>Platformer Academy</strong></h4>
    </a>
    <nav role="navigation" class="nav-menu-2 w-nav-menu">
    <?php get_template_part( 'template-parts/nav/plat_nav_out'); ?>
    </nav>
    <div class="menu-button-2 w-nav-button">
      <div class="w-icon-nav-menu"></div>
    </div>
  </div>
</div>