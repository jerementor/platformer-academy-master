<?php 

/**
 * The single file for the tutorial custom post type
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Platformer
 * @since 1.0.0
 */

get_header(); 

?>
<div class="plat_minvh_section plat_padding_section plat_video_lecture">
  <?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
      <div class="div-block-11">
            <div class="w-container">
              <div style="padding-top:56.27659574468085%;" class="plat_video w-embed w-video">
                <?php the_content(); ?>
              </div>
            </div>
          </div>
      <div class="w-container">
            <div class="div-block-12">
              <div class="w-row">
                <div class="w-col w-col-6">
                  <div>
                    <h3 class="plat_generic_subheader"><?php wp_title(''); ?></h3>
                  </div>
                  <p class="plat_paragraph"><?php the_excerpt(); ?></p>
      
                </div>
                <div class="w-col w-col-6">
                  <div>
                    <div class="plat_boxed_div">
                      <a href="/tutorials" class="plat_lead_button_green w-button">
                        Back To Tutorials</a>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>  
    <?php endwhile; ?>
  <?php endif; ?>            
</div>
<!--Close Section-->
<?php get_footer(); ?>
