<?php 

/**
 * The archive file
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Platformer
 * @since 1.0.0
 */

get_header(); 

?>

 <div class="plat_minvh_section plat_padding_section">
    <div class="w-container">
        <div class="plat_content_div">
            <h1 class="plat_template_header"><?php wp_title(''); ?></h1>
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
           <h3><?php the_title(sprintf('<a class="plat_content_header" href="%s" rel="bookmark">', esc_url(get_permalink() )), '</a>'); ?></h3>

            <div class="plat_paragraph">
                <?php the_excerpt(); ?>
            </div>
                <?php endwhile; ?>
            <?php endif; ?>  
            <div id="pagination" class="plat_pagination">
                <?php posts_nav_link(); ?>
            </div>
    </div>
</div>


<?php get_footer(); ?>

