<?php 

/**
 * The template for displaying all pages
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<body>
  <div class="plat_minvh_section plat_padding_section">
    <div class="w-container">
     <div class="plat_page_header">
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>

        <?php the_author ?>
            <h1 class="plat_blog_header"><?php wp_title(''); ?></h1>
            <h4 class="plat_date"><?php the_time( get_option( 'date_format' ) ); ?></h4>
            <h4 class="plat_author"><?php the_author_meta('display_name', 2); ?></h4>
            <div class="plat_paragraph w-richtext">
                <?php the_content(); ?>
            </div>

        <?php endwhile; ?>
    <?php endif; ?>    
    </div>
    <!--End Page Header-->
    </div> 
    <!--End Container-->
    </div>
    <!--End Section-->
     
    
<?php get_footer(); ?>

