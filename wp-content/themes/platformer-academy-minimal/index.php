<?php 

/**
 * The template for displaying all pages
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<body>
  <div class="plat_minvh_section plat_padding_section">
    <div class="w-container">
      <div class="plat_content_div">
            <h1 class="plat_template_header"><?php wp_title(''); ?></h1>
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
           <?php the_title(sprintf('<a class="plat_content_header" href="%s" rel="bookmark">', esc_url(get_permalink() )), '</a>'); ?>

            <div class="plat_paragraph">
                <?php the_excerpt(); ?>
            </div>

        <?php endwhile; ?>
    <?php endif; ?>    

    </div>
    <!--End Content Div-->
    </div> 
    <!--End Container-->
    </div>
    <!--End Section-->
     
 
     
    
<?php get_footer(); ?>

