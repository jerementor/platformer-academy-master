<?php
/**
 * The sidebar containing the course menu for Discover Game Design
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Platformer
 * @version 1.0.0
 */
?>
<?php if(is_page_template('page-all-lectures.php') ) : ?>

  <?php if(is_active_sidebar('course-sidebar')) : ?>
      <aside id="secondary" class="unordered-list-4 w-list-unstyled" role="complementary">
         <?php dynamic_sidebar('course-sidebar'); ?>
      </aside>
  <?php endif; ?>
<?php endif; ?>