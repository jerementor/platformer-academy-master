<?php 

/**
 * Template Name: Lecture Page
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<body>
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
        <div class="plat_minvh_section plat_padding_section plat_video_lecture">
        <div class="plat_video_background">
          <div class="w-container">
              <div style="padding-top:56.27659574468085%;" class="plat_video w-embed w-video">
                <?php the_excerpt(); ?>
              </div>
            </div>
        </div>

        
        <div class="w-container">
            <div class="plat_lecture_content">
                  <?php the_content(); ?>
            </div>
        </div>
        </div>
        <?php endwhile; ?>
    <?php endif; ?>            

     
    
<?php get_footer(); ?>
 