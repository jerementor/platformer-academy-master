<?php 

/**
 * Template Name: Landing Page Template
 *
 *
 * @package Platformer
 * @since 1.0.0
 */

get_header();

?>
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            
           <?php the_content(); ?>    
           <!--<@?php get_template_part('template-parts/page/main/page-home'); ?>-->

        <?php endwhile; ?>
    <?php endif; ?>     
     
  

<?php get_footer(); ?>
 