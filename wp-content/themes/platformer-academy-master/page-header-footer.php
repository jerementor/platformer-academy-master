<?php 

/**
 * Template Name: Header/Footer Empty
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<body>

     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            
            <?php the_content(); ?>

        <?php endwhile; ?>
    <?php endif; ?>            

     
    
<?php get_footer(); ?>