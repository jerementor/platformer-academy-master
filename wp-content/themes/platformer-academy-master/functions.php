<?php

//Loading in Scripts
function plat_enqueue_styles(){
    wp_enqueue_script('webflow-js', get_template_directory_uri() . '/assets/js/webflow.js', array('jquery'));

    
    wp_enqueue_style("normalize",  get_template_directory_uri() . "/assets/css/normalize.css");
    wp_enqueue_style("webflow",  get_template_directory_uri() . "/assets/css/webflow.css");
    wp_enqueue_style("platformeracademy",  get_template_directory_uri() ."/assets/css/platformer-academy-wordpress.webflow.css");
    wp_enqueue_style("platformeracademy-style", get_stylesheet_uri());
    
}
add_action("wp_enqueue_scripts", "plat_enqueue_styles");

function plat_menu_setup(){
    
    //Registers custom primary nav menu 
    register_nav_menus( array(
        'primary' => __("Primary Menu", "platformeracademy"),
        'login' => __("Login Menu", "platformeracademy-login"),
        'course' => __("Course Sidebar", "platformeracademy-course-sidebar"),
    ));
    
}
add_action("after_setup_theme", "plat_menu_setup");

function platformeracademy_setup(){
    	/*
	 * Enable support for Post Formats.
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );
}
add_action( 'after_setup_theme', 'platformeracademy_setup' );


function plat_class_to_excerpt( $excerpt) {
  if(is_singular('tutorial')) {
      return str_replace('<p', '<p class="plat_full_paragraph"', $excerpt);
  } else {
      return str_replace('<p', '<p class="plat_paragraph plat_content_paragraph"', $excerpt);
  }
}
add_filter("the_excerpt", "plat_class_to_excerpt");

// function plat_custom_shortcode( $atts, $content = null) {
//   return '<div class="w-col">'. $content . '</div>';
// }

remove_filter('the_content', 'wpautop');

function plat_widgets(){
  register_sidebar(array(
     'name'           => __('Discover Game Design Sidebar', 'platformeracademy'),
     'id'             => 'course-sidebar-dgd',
     'description'    => __('Add widgets here to appear in the sidebar', 'platformeracademy'),
     'before_widget'  => '<div id="%1$s" class="widget %2$s">',
     'after_widget'   => '</div>',
     'before_title'   => '<h3 class="plat_content_header plat_course_sidebar_header">',
     'after_title'    => '</h3>'
  ));
  
  register_sidebar(array(
     'name'           => __('Prototype to Demo Sidebar', 'platformeracademy'),
     'id'             => 'course-sidebar-proto',
     'description'    => __('Add widgets here to appear in the sidebar', 'platformeracademy'),
     'before_widget'  => '<div id="%1$s" class="widget %2$s">',
     'after_widget'   => '</div>',
     'before_title'   => '<h3 class="plat_content_header plat_course_sidebar_header">',
     'after_title'    => '</h3>'
  ));
  
  register_sidebar(array(
     'name'           => __('Enhance Your Platformer Sidebar', 'platformeracademy'),
     'id'             => 'course-sidebar-enhance',
     'description'    => __('Add widgets here to appear in the sidebar', 'platformeracademy'),
     'before_widget'  => '<div id="%1$s" class="widget %2$s">',
     'after_widget'   => '</div>',
     'before_title'   => '<h3 class="plat_content_header plat_course_sidebar_header">',
     'after_title'    => '</h3>'
  ));

  register_sidebar(array(
     'name'           => __('Action Platformer Sidebar', 'platformeracademy'),
     'id'             => 'course-sidebar-action',
     'description'    => __('Add widgets here to appear in the sidebar', 'platformeracademy'),
     'before_widget'  => '<div id="%1$s" class="widget %2$s">',
     'after_widget'   => '</div>',
     'before_title'   => '<h3 class="plat_content_header plat_course_sidebar_header">',
     'after_title'    => '</h3>'
  ));
    
}
add_action('widgets_init', 'plat_widgets');



?>

