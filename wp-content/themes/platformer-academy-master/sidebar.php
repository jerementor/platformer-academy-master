<?php
/**
 * The sidebar containing the course menu for Discover Game Design
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Platformer
 * @version 1.0.0
 */
?>
<?php if(is_page_template('page-course-dgd.php') ) : ?>

  <?php if(is_active_sidebar('course-sidebar-dgd')) : ?>
      <aside id="secondary" class="w-list-unstyled" role="complementary">
         <?php dynamic_sidebar('course-sidebar-dgd'); ?>
      </aside>
  <?php endif; ?>

<?php elseif (is_page_template('page-course-eyp.php')) : ?>

  <?php if(is_active_sidebar('course-sidebar-enhance')) : ?>
      <ul id="sidebar" class="w-list-unstyled">
         <?php dynamic_sidebar('course-sidebar-enhance'); ?>
      </ul>
  <?php endif; ?>
  
<?php elseif (is_page_template('page-course-ptd.php')) : ?>

  <?php if(is_active_sidebar('course-sidebar-proto')) : ?>
      <ul id="sidebar" class="w-list-unstyled">
         <?php dynamic_sidebar('course-sidebar-proto'); ?>
      </ul>
  <?php endif; ?>  
  
<?php elseif (is_page_template('page-course-acp.php')) : ?>

  <?php if(is_active_sidebar('course-sidebar-action')) : ?>
     <ul id="sidebar" class="w-list-unstyled">
        <?php dynamic_sidebar('course-sidebar-action'); ?>
     </ul>
 <?php endif; ?>     
 
<?php endif; ?>
 

 


