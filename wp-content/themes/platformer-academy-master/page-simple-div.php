<?php 


/**
 * Template Name: Simple Page Template
 *
 *
 * @package Platformer
 * @since 1.0.0
 */

get_header(); 

?>
<body>
    <div class="plat_minvh_section plat_background_whitesmoke">
    <div class="w-container">
        <h1 class="plat_page_header"><?php wp_title('') ?></h1>
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            
           <!--<!?php the_title(sprintf('<a class="plat_content_header" href="%s" rel="bookmark">', esc_url(get_permalink() )), '</a>'); ?>-->
           <div class="plat_normal_div plat_div_white">
            <?php the_content(); ?>
           </div>
        <?php endwhile; ?>
    <?php endif; ?>            
    </div>
    </div>
     
    
<?php get_footer(); ?>