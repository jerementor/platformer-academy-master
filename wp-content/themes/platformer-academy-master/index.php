<?php 

/**
 * The main template file
 * Required if WP has no other choice
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<body>
    <div class="plat_minvh_section plat_background_whitesmoke">
    <div class="w-container">
        <h1 class="plat_page_header"><?php wp_title('') ?></h1>
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            
           <?php the_title(sprintf('<a class="plat_content_header" href="%s" rel="bookmark">', esc_url(get_permalink() )), '</a>'); ?>
           <?php the_excerpt(); ?>

            
        <?php endwhile; ?>
    <?php endif; ?>            
    </div>
    </div>
     
    
<?php get_footer(); ?>