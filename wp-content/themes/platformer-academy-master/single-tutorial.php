<?php 

/**
 * The single file for the tutorial custom post type
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Platformer
 * @since 1.0.0
 */

get_header(); 

?>

<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="plat_background_dark plat_base_section plat_tutorial_section">
  <div class="w-container">
    <div class="plat_video_link w-embed w-video" style="padding-top: 56.17021276595745%;">
          <?php the_content(); ?>
    </div>
  </div>
</div>
<div class="plat_background_whitesmoke plat_base_section">
  <div class="w-container">
      <div class="plat_div_white plat_normal_div">
        <h1 class="plat_lead_header"><?php wp_title('') ?></h1>
       <p class="plat_full_paragraph"><?php the_excerpt(); ?></p>
    <div class="plat_tutorial_button_div">
        <a class="plat_lead_button w-button" href="/tutorials">Back To Tutorials</a>
      </div>       
     </div>
  
  </div>
</div>
<?php endwhile; ?>
<?php endif; ?>            

<?php get_footer(); ?>