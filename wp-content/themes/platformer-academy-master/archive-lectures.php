<?php 

/**
 * The archive file for the tutorial custom post type
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Platformer
 * @since 1.0.0
 */

get_header(); 

?>
<div class="plat_minvh_section plat_background_whitesmoke">
    <div class="w-container">
        <h1 class="plat_page_header"><?php wp_title('') ?></h1>
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
            <div class="plat_content_div">
                    <?php the_title(sprintf('<a class="plat_content_header" href="%s" rel="bookmark">', esc_url(get_permalink() )), '</a>'); ?>
                    <?php the_excerpt(); ?>
            </div>    
                <?php endwhile; ?>
            <?php endif; ?>  
            <div id="pagination" class="plat_pagination">
                <?php posts_nav_link(); ?>
            </div>
    </div>
</div>

<?php get_footer(); ?>