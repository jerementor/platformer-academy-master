<?php 

/**
 * Template Name: Sales Page
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<body>
    <div class="plat_minvh_section plat_background_whitesmoke">
    <div class="w-container">
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            
            <?php the_content(); ?>

        <?php endwhile; ?>
    <?php endif; ?>            
    </div>
    </div>
     
    
<?php get_footer(); ?>
 