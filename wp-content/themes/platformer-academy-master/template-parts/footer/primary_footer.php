<footer class="plat_footer">
  <div class="w-container">
    <div class="w-row">
      <div class="w-col w-col-6">
        <div class="plat_footer_copyright"><span class="plat_footer_bold">Copyright &copy; 2017 Platformer Academy<br xmlns="http://www.w3.org/1999/xhtml"></span><span class="plat_footer_light">If Else Learn LLC, PO Box 11 Millburn, NJ 07041</span></div>
      </div>
      <div class="w-col w-col-6">
        <div class="plat_footer_links">
         <?php
            $defaults = array(
                'theme_location'  => 'primary',
                'menu'            => '',
                'container'       => '',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => '',
                'menu_id'         => '',
                'echo'            => false,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<nav id="%1$s" class="%2$s">%3$s </nav>',
                'depth'           => 0,
                'walker'          => ''
            );
            
            $find = array('><a', '<li');
            $replace = array('', '<a class="plat_footer_link"');
            
            echo str_replace($find, $replace, wp_nav_menu( $defaults ));
            ?> 
        </div>
      </div>
    </div>
  </div>
</footer>
