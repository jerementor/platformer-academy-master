 // $defaults = array(
    //     'theme_location'  => 'course',
    //     'menu'            => '',
    //     'container'       => '',
    //     'container_class' => '',
    //     'container_id'    => '',
    //     'menu_class'      => '',
    //     'menu_id'         => '',
    //     'echo'            => false,
    //     'fallback_cb'     => 'wp_page_menu',
    //     'before'          => '',
    //     'after'           => '',
    //     'link_before'     => '',
    //     'link_after'      => '',
    //     'items_wrap'      => '<nav id="%1$s" class="%2$s">%3$s </nav>',
    //     'depth'           => 0,
    //     'walker'          => ''
    // );
    
    // $find = array('><a', '<li');
    // $replace = array('', '<ul class="w-list-unstyled"> <li class="list-item plat_course_sidebar_li"> <a class="plat_lecture_text plat_nav_link"');
    
    // echo str_replace($find, $replace, wp_nav_menu( $defaults ));
 

.menu-item {
    margin-top: 8px;
    margin-bottom: 8px;
    border-top: 1px solid #cfcfd6;
    border-bottom: 1px solid #cfcfd6;
    border-top-style: none;
    list-style-type: none;
    text-align: left;
}

.menu-item > a {
  color: #3c3c4a;
  font-size: 17px;
  line-height: 28px;
  font-weight: 500;
  text-decoration: none;
  opacity: 0.5;
  -webkit-transition: opacity 200ms ease;
  transition: opacity 200ms ease;
  font-size: 16px;
  font-weight: 700;  
}

.menu-item > a:hover {
  opacity: 1;
}