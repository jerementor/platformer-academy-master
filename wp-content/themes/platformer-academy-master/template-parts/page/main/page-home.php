<?php 

/**
 * Template Name: Home Page (Static)
 *
 *
 * @package Platformer
 * @since 1.0.0
 */


?>
<div class="plat_home_main_section">
<div class="plat_home_navbar w-nav" data-animation="default" data-collapse="medium" data-duration="400">
  <div class="w-container">
    <a class="plat_brand w-nav-brand" href="index.html">
      <h1 class="plat_logo_text">Platformer Academy</h1>
    </a>
    <nav class="nav-menu w-nav-menu" role="navigation">
        <a class="plat_nav_link w-nav-link" href="/articles">Articles</a>
        <a class="plat_nav_link w-nav-link" href="/tutorials">Tutorials</a>
        <a class="plat_nav_link w-nav-link" href="/series">Series</a>
        <a class="plat_nav_link w-nav-link" href="/products">Products</a>
    </nav>
    <div class="plat_mobile_menu w-nav-button">
      <div class="plat_mobile_icon w-icon-nav-menu"></div>
    </div>
  </div>
</div>
<div class="plat_home_main_content">
  <div class="w-container">
    <div class="w-row">
      <div class="w-col w-col-6">
        <h1 class="plat_home_welcome">Welcome</h1>
        <h1 class="plat_home_subheader">Let's <span class="text-span">Enhance Your Platformer</span></h1>
        <div class="plat_home_course_text">Sign up for the new e-mail course and take your Platformer game from&nbsp;<em>nothing</em>&nbsp;to&nbsp;<strong>something</strong>.</div>
        <div class="plat_home_course_light plat_home_course_text">*This course uses Construct 2/3 to showcase general logic principles that can be applied to any software</div>
      </div>
      <div class="w-col w-col-6"></div>
    </div>
  </div>
</div>
</div>
<div class="plat_background_whitesmoke plat_base_section">
<div class="w-container">
  <div class="w-row">
    <div class="w-col w-col-4">
      <h1 class="plat_card_header">Articles</h1>
      <p class="plat_paragraph_center plat_paragraph_small plat_paragraph_small_light">Read our articles on every aspect of game development</p>
    </div>
    <div class="w-col w-col-4">
      <h1 class="plat_card_header">Tutorials</h1>
      <p class="plat_paragraph_center plat_paragraph_small plat_paragraph_small_light">Watch over 50 tutorials on programming your platformer</p>
    </div>
    <div class="w-col w-col-4">
      <h1 class="plat_card_header">Series</h1>
      <p class="plat_paragraph_center plat_paragraph_small plat_paragraph_small_light">Kick back, relax and binge watch our&nbsp;step-by-step series</p>
    </div>
  </div>
</div>
</div>
<div class="plat_base_section plat_top_bottom_section">
<div class="w-container">
  <h1 class="plat_lead_header plat_page_header">Enhance Your Platformer</h1>
  <div class="plat_paragraph plat_paragraph_padding w-richtext">

  </div>
</div>
</div>