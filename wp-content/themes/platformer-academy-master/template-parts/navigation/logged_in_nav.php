<div class="plat_secondary_nav">
<div class="w-container">
  <div class="plat_secondarynav_div w-clearfix">
    <h3 class="heading-2">Hello, <?php do_shortcode('[accessally_user_firstname]') ?></h3>
    
    <?php
      $defaults = array(
          'theme_location'  => 'login',
          'menu'            => '',
          'container'       => '',
          'container_class' => '',
          'container_id'    => '',
          'menu_class'      => '',
          'menu_id'         => '',
          'echo'            => false,
          'fallback_cb'     => 'wp_page_menu',
          'before'          => '',
          'after'           => '',
          'link_before'     => '',
          'link_after'      => '',
          'items_wrap'      => '<nav id="%1$s" class="%2$s">%3$s </nav>',
          'depth'           => 0,
          'walker'          => ''
      );
      
      $find = array('><a', '<li');
      $replace = array('', '<a class="plat_nav_dark_link plat_nav_link"');
      
      echo str_replace($find, $replace, wp_nav_menu( $defaults ));
    ?>     
    <!--<a class="plat_nav_dark_link plat_nav_link" href="#">Dashboard</a>-->
    <!--<a class="plat_nav_dark_link plat_nav_link plat_snav_padding" href="#">My Account</a>-->
    <!--<a class="plat_nav_dark_link plat_nav_link plat_snav_padding" href="#">Logout</a>-->
  </div>
</div>
</div>