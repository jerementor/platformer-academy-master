<div class="w-container">
  <a class="plat_brand w-nav-brand" href="/">
    <h1 class="plat_logo_text">Platformer Academy</h1>
  </a>
  <nav class="nav-menu w-nav-menu" role="navigation">
  <?php
    $defaults = array(
        'theme_location'  => 'primary',
        'menu'            => '',
        'container'       => '',
        'container_class' => '',
        'container_id'    => '',
        'menu_class'      => '',
        'menu_id'         => '',
        'echo'            => false,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<nav id="%1$s" class="%2$s">%3$s </nav>',
        'depth'           => 0,
        'walker'          => ''
    );
    
    $find = array('><a', '<li');
    $replace = array('', '<a class="plat_nav_link w-nav-link"');
    
    echo str_replace($find, $replace, wp_nav_menu( $defaults ));
    ?>        
  </nav>
  <div class="plat_mobile_menu w-nav-button">
    <div class="plat_mobile_icon w-icon-nav-menu"></div>
  </div>
</div>
</div>

  