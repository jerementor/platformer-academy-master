<?php 

/**
 * The single template
 *
 * @package Platformer
 * @since 1.0.0
 */

get_header(); 

?>
<body>
    <div class="plat_background_dark plat_base_section plat_tutorial_section">
        <div class="w-container">
          <h1 class="plat_page_header_inverse"><?php wp_title('')?></h1>
        </div>
    </div>
  <div class="plat_post_section">
    <div class="w-container">
    <div class="plat_minvh_section">
      <div class="plat_div_white plat_normal_div">    
         <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <p class="plat_paragraph"><?php the_content(); ?></p>  
            <?php endwhile; ?>
        <?php endif; ?>                 
        </div>
       </div>
    </div>
    </div>  

     
    
<?php get_footer(); ?>