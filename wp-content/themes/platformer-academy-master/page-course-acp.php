<?php 

/**
 * Template Name: Course Page -- ACP
 *
 *
 * @package Platformer
 * @since 1.0.0
 */

get_header();

?>
  <?php if ( have_posts() ) : ?>
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="plat_flex_course_section plat_flex_section plat_minvh_section">
        <div class="plat_course_sidebar">
         <h1 class="plat_course_page_header plat_page_header">Table of Contents</h1>
            <div class="plat_sidebar_lecture_section">               
                <?php get_sidebar('course-sidebar-action'); ?>
          </div>
          </div>
        <div class="plat_course_content">
            <div class="w-container">
                <?php the_content(); ?>    
            </div>
          </div>
      <?php endwhile; ?>
  <?php endif; ?>     
<?php get_footer(); ?>
 