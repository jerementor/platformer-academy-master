<?php
/**
 * The template for displaying the footer
 *
 *
 * @package Platformer
 * @version 1.0
 */
if(!is_page_template('page-course-dgd.php' || 'page-course-eyp.php' || 'page-course-acp.php' || 'page-course-ptd.php') ) {
  get_template_part('template-parts/footer/primary_footer');
}

if(is_front_page()){
  get_template_part('template-parts/footer/primary_footer');
}

if(is_page_template('page-header-footer.php')) {
  get_template_part('template-parts/footer/primary_footer');
}

?>


<?php wp_footer(); ?>
</body>
</html>