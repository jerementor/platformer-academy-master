<?php 

/**
 * Template Name: Dashboard Page Template
 *
 *
 * @package Platformer
 * @since 1.0.0
 */

get_header();

?>
 <div class="plat_background_whitesmoke plat_minvh_section">
    <div class="w-container">
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            
           <?php the_content(); ?>    

        <?php endwhile; ?>
    <?php endif; ?>     
     
      </div>
    </div>

<?php get_footer(); ?>
 