<?php
/**
 * The sidebar containing the course menu
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Platformer
 * @version 1.0.0
 */
?>

<?php if(is_page_template('page-lecture.php' || 'page-lecture-text.php') ) : ?>
  <?php if(is_active_sidebar('course-sidebar')) : ?>
   
    <div  data-w-id="dd9e606c-b657-674b-31c8-40b9235d0250" class="course_sidebar">
        <aside id="secondary" role="complementary">
             <h4 class="centered header3">Table of Contents</h4>
             <div class="section_title">
                 <?php dynamic_sidebar('course-sidebar'); ?>
             </div>
        </aside>   
    </div>

  <?php endif; ?>
<?php endif; ?>

