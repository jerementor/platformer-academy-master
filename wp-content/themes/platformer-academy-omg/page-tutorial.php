<?php 


/**
 * Template Name: Tutorial Template
 *
 *
 * @package Platformer
 * @since 1.0
 */


get_header(); 

?>

<div class="w-container">
    <div class="content_area">
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="embed-container">
               <?php the_field('o_video'); ?> 
            </div>
        </div>

            <div class="content_card_desc">
                <?php the_content(); ?>
                <a href="/tutorials" class="content_button w-button">Back to tutorials</a>
            </div>
        
        <?php endwhile; ?>
    <?php endif; ?>   
</div>
<!--End content_area-->
</div>
<!--End Container-->
     
    
<?php get_footer(); ?>

