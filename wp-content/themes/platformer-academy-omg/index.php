<?php 

/**
 * The template for displaying all pages
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<!--INDEX-->
<div class="w-container">
    <div class="content_area">
    <h3 class="header4"><?php wp_title('Recent'); ?></h3>
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post();  ?>
              <ul class="content_list">
            <li class="content_blog_li w-clearfix">
              <div class="content_title">
                <?php the_title(sprintf('<a class="content_link link" href="%s" rel="bookmark">', esc_url(get_permalink() )), '</a>'); ?>
              </div>
              <div class="content_type">
                <div><?php the_excerpt(); ?></div>
              </div>
            </li>
</ul>
        <?php endwhile; ?>
    <?php endif; ?>   
</div>
<!--End content_area-->
</div>
<!--End Container-->
     
    
<?php get_footer(); ?>

