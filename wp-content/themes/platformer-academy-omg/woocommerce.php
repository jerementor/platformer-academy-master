<?php 

/**
 * The template for displaying all pages
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<!--USING WOOCOMMERCE.PHP-->
<div class="w-container">
    <div class="content_area">
          <?php woocommerce_content(); ?>
</div>
<!--End content_area-->
</div>
<!--End Container-->
     
    
<?php get_footer(); ?>

