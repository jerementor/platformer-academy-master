<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta content="width=device-width, initial-scale=1" name="viewport">      
    <title><?php wp_title(''); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Libre Baskerville:regular,italic,700","Raleway:regular","Arvo:regular","Roboto:regular","Poppins:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Playfair Display:regular,italic,700,700italic,900,900italic","Roboto Slab:100,300,regular,700","Abril Fatface:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
    <!-- Deadline Funnel --><script type="text/javascript" data-cfasync="false">function base64_encode(e){var r,t,c,a,h,n,o,A,i = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",d=0,l=0,s="",u=[];if(!e)return e;do r=e.charCodeAt(d++),t=e.charCodeAt(d++),c=e.charCodeAt(d++),A=r<<16|t<<8|c,a=A>>18&63,h=A>>12&63,n=A>>6&63,o=63&A,u[l++]=i.charAt(a)+i.charAt(h)+i.charAt(n)+i.charAt(o);while(d<e.length);s=u.join("");var C=e.length%3;var decoded = (C?s.slice(0,C-3):s)+"===".slice(C||3);decoded = decoded.replace("+", "-");decoded = decoded.replace("/", "_");return decoded;} var url = base64_encode(location.href);var parentUrl = (parent !== window) ? ("/" + base64_encode(document.referrer)) : "";(function() {var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.setAttribute("data-scriptid", "dfunifiedcode");s.src ="https://deadlinefunnel.com/unified/eyJpdiI6IjRPUUs2M294WVRPeVdzczBVV1c3UXc9PSIsInZhbHVlIjoiNmdQUFRmUjd6bTNvQmVUQmNVY1wvbnc9PSIsIm1hYyI6ImI2NDZlNWY3NjQxODFlZWZiNjU0YWI3YzcxYjI5NzUyOTM0NjJlOGJlNjk1MGFjOGZhYjk3YWVlZWE3NjY4NjUifQ==/"+url+parentUrl;var s2 = document.getElementsByTagName("script")[0];s2.parentNode.insertBefore(s, s2);})();</script><!-- End Deadline Funnel -->


    <?php wp_head(); ?>
    
    
</head>

<body>

<?php if (is_user_logged_in() ) : ?>
    <?php get_template_part('template-parts/nav/logged_in_nav'); ?>
<?php else: ?>
    <?php get_template_part('template-parts/nav/logged_out_nav'); ?>
<?php endif; ?>    
    
    
<?php if(is_page_template('page-lecture.php' || 'page-lecture-text.php') ) : ?>
    <div class="icon" data-ix="close-sidebar">
      <div class="icon_close">Close</div>
      <div class="icon_open">Open Menu</div>
    </div>
<?php endif; ?>   



    <div class="main_container">
        
        <?php get_sidebar(); ?> 
