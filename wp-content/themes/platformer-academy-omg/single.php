<?php 

/**
 * The template for displaying all pages
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<!--SINGLE-->
<div class="fullwidth_div">
<div class="w-container">
  <h1 class="left_header"><?php wp_title(''); ?></h1>
  <h3 class="left_sub_header"><?php the_excerpt(); ?></h3>
</div>
</div>

<div class="w-container">
<div class="content_area">
<div class="w-row">
<div class="w-col w-col-4">
    <?php the_post_thumbnail( 'medium' ); ?>    
            <!--get feature dimage-->
            <!--<img src="images/skeleton_only.png" width="175" class="image-2">-->
          <!--<div class="text-block-7">Skeleton Framework Logo</div>-->
         <!--<blockquote class="block-quote">&quot;<em><strong>Jeremy</strong> has some of the most comprehensive and succinct</em> <em>tutorials out there! As a pixel artist who has wanted to make my own games for a long time–but with no coding experience–Jeremy&#x27;s courses helped me gain the confidence I needed to make my own art come to life!</em>&quot;<br><br>– <strong class="bold-text">Sandy Gordon (Pixel Artist)</strong></blockquote>-->
</div>
<div class="w-col w-col-8">
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
          <!--<div class="paragraph w-richtext">-->
               <?php the_content(); ?>
          <!--</div>-->
        <?php endwhile; ?>
    <?php endif; ?>   
  </div>
</div>
 

     
    
<?php get_footer(); ?>

