<?php 

/**
 * The template for displaying series
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<!--SINGLE SERIES-->
<div class="w-container">
    <div class="content_area">
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
             
          <div class="w-container">
                <?php the_content(); ?>
          </div>    

        <?php endwhile; ?>
    <?php endif; ?>   
</div>
<!--End content_area-->
</div>
<!--End Container-->
     
    
<?php get_footer(); ?>

