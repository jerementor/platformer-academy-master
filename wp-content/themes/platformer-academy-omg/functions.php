<?php

//Loading in Scripts
function plat_enqueue_styles(){
    wp_enqueue_script('webflow-js', get_template_directory_uri() . '/assets/js/webflow.js', array('jquery'));

    
    wp_enqueue_style("normalize",  get_template_directory_uri() . "/assets/css/normalize.css");
    wp_enqueue_style("webflow",  get_template_directory_uri() . "/assets/css/webflow.css");
    wp_enqueue_style("platformeracademy",  get_template_directory_uri() ."/assets/css/platformer-academy-omg.webflow.css");
    wp_enqueue_style("platformeracademy-style", get_stylesheet_uri());
    
}
add_action("wp_enqueue_scripts", "plat_enqueue_styles");

function plat_menu_setup(){
    
    //Registers custom primary nav menu 
    register_nav_menus( array(
        'primary' => __("Primary Menu", "platformeracademy"),
        'logged-in' => __("Logged-In Menu", "platformeracademy-login"),
        'logged-out' => __("Logged-Out Menu", "platformeracademy-logout"),
    ));
    
}
add_action("after_setup_theme", "plat_menu_setup");

function platformeracademy_setup(){
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	
	) );
}
add_action( 'after_setup_theme', 'platformeracademy_setup' );


// remove_filter('the_content', 'wpautop');
add_post_type_support( 'page', 'excerpt' );

function add_custom_post_type_to_query( $query ) {
    if ( $query->is_home() && $query->is_main_query() ) {
        $query->set( 'post_type', array('post', 'video') );
    }
}
add_action( 'pre_get_posts', 'add_custom_post_type_to_query' );


function plat_widgets(){
  register_sidebar(array(
     'name'           => __('Course One - Sidebar', 'platformeracademy'),
     'id'             => 'course-sidebar',
     'description'    => __('Add widgets here to appear in the sidebar', 'platformeracademy'),
     'before_widget'  => '<div id="%1$s" class="%2$s">',
     'after_widget'   => '</div>',
     'before_title'   => '<h4 class="section_title_header"> ',
     'after_title'    => '</h4>',
  ));
  
  
    
}
add_action('widgets_init', 'plat_widgets');


// WooCommerce Support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}




?>

