<?php 

/**
 * The template for displaying all pages
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<!--ARCHIVE SERIES-->
<div class="w-container">
    <div class="content_area">
    <h3 class="header4"><?php wp_title('All'); ?></h3>
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post();  ?>
        <a href="/" class="content_card_link w-inline-block">
            <div class="content_card">
              <h4 class="header3 main_underline"><?php the_title(); ?></h4>
              <div><?php wp_count_posts( 'Lectures' ) ?></div>
            </div>
         </a>
        <?php endwhile; ?>
    <?php endif; ?>   
</div>
<!--End content_area-->
</div>
<!--End Container-->
     
    
<?php get_footer(); ?>

