<?php 

/**
 * The template for displaying all pages
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<!--USING PAGE.PHP-->
<div class="w-container">
    <div class="content_area">
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
           <!--<!?php the_title(sprintf('<a class="plat_content_header" href="%s" rel="bookmark">', esc_url(get_permalink() )), '</a>'); ?>-->
                <?php the_content(); ?>
        <?php endwhile; ?>
    <?php endif; ?>   
</div>
<!--End content_area-->
</div>
<!--End Container-->
     
    
<?php get_footer(); ?>

