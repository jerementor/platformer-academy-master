<?php 


/**
 * Template Name: Lecture Template
 *
 *
 * @package Platformer
 * @since 1.0
 */


get_header(); 

?>
<!--LECTURE TEMPLATE-->
<div class="w-container">
    <div class="content_area">
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
             <div class="embed-container">
                <?php the_field('o_video'); ?> 
             </div>

            <div class="content_card_desc">
             <h4 class="header3 main_underline"><?php wp_title('') ?></h4>
            
                <?php the_content(); ?>
            </div>
        
        <?php endwhile; ?>
    <?php endif; ?>   
</div>
<!--End content_area-->
</div>
<!--End Container-->
     
    
<?php get_footer(); ?>


