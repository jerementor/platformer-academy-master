<?php 

/**
 * The template for displaying videos
 *
 *
 * @package Platformer
 * @since 1.0
 */

get_header(); 

?>
<!--SINGLE VIDEO-->
<div class="w-container">
    <div class="content_area">
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
                <?php the_field('o_video'); ?> 
            <div class="content_card_desc">
                <?php the_content(); ?>
                <a href="/tutorials" class="content_button w-button">Back to tutorials</a>
            </div>
        
        <?php endwhile; ?>
    <?php endif; ?>   
</div>
<!--End content_area-->
</div>
<!--End Container-->
     
    
<?php get_footer(); ?>

