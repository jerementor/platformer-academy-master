<?php 


/**
 * Template Name: Dashboard Template
 *
 *
 * @package Platformer
 * @since 1.0
 */


get_header(); 

?>
<!--USING PAGE-DASHBOARD.PHP-->
<div class="w-container">
    <div class="content_area">
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>

        <!--<div data-duration-in="300" data-duration-out="100" class="w-tabs">-->
        <!--    <div class="w-tab-menu">-->
        <!--      <a data-w-tab="Tab 1" class="tab_link w--current w-inline-block w-tab-link">-->
        <!--        <div>Your Courses</div>-->
        <!--      </a>-->
        <!--      <a data-w-tab="Tab 2" class="tab_link w-inline-block w-tab-link">-->
        <!--        <div>Your Account</div>-->
        <!--      </a>-->
        <!--    </div>-->
        <!--    </div>    -->
        <?php endwhile; ?>
    <?php endif; ?>   
</div>
<!--End content_area-->
</div>
<!--End Container-->
     
    
<?php get_footer(); ?>



