<?php 


/**
 * Template Name: Text Lecture Template
 *
 *
 * @package Platformer
 * @since 1.0
 */


get_header(); 

?>
<!--LECTURE TEMPLATE-->
<div class="w-container">
    <div class="content_area">
     <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
        <div class="blog_meta grey_div">
            <h2 class="blog_title header2"><?php wp_title(''); ?></h2>
            <h4 class="header4_author">By Jeremy Alexander</h4>
          </div>
            <div class="blog_content">
                <div class="w-richtext">        
                    <?php the_content(); ?>
                </div>
            </div> 
        <?php endwhile; ?>
    <?php endif; ?>

</div>
<!--End content_area-->
</div>
<!--End Container-->
     
    
<?php get_footer(); ?>


