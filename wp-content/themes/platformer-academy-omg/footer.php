 <div class="footer">
            <div class="text-block">Copyright &copy; Platformer Academy by If Else Learn, LLC<br>P.O Box 11 Millburn Ave, NJ 07041</div>
</div>
<script type="text/javascript">
      var _dcq = _dcq || [];
      var _dcs = _dcs || {};
      _dcs.account = '4164281';
    
      (function() {
      var dc = document.createElement('script');
      dc.type = 'text/javascript'; dc.async = true;
      dc.src = '//tag.getdrip.com/4164281.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(dc, s);
      })();
</script>
<!--End Drip-->
<!-- RightMessage -->
<script type="text/javascript">
(function(p, a, n, d, o) {
    a = document.createElement('script');
    a.type = 'text/javascript'; a.async = true; a.src = p;
    n = document.getElementsByTagName('script')[0]; n.parentNode.insertBefore(a, n);
})('https://tag.rightmessage.io/127528207.js');
</script>
<!--End RightMessage-->
<?php wp_footer(); ?>
</div> 
<!--Close main_container-->

</body>
</html>